/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author olive
 */
public class Puntaje extends javax.swing.JFrame {

     String c;
     
    public Puntaje() {
        initComponents();
          this.setLocationRelativeTo(null);
          
         ImageIcon ruta = new ImageIcon("src/multimedia/evaluacion.png");
        this.setIconImage(ruta.getImage());
  
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        radios2 = new javax.swing.ButtonGroup();
        jPanel4 = new javax.swing.JPanel();
        combo = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        chkIngci = new javax.swing.JCheckBox();
        chkIngind = new javax.swing.JCheckBox();
        chkGeren = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        r1 = new javax.swing.JRadioButton();
        r2 = new javax.swing.JRadioButton();
        r3 = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        txtTOL = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        boton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Evaluacion de empleado");
        setBackground(new java.awt.Color(0, 102, 102));
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel4.setBackground(new java.awt.Color(0, 102, 102));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Experiencia profesional", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 1, 18), new java.awt.Color(0, 0, 0))); // NOI18N

        combo.setBackground(new java.awt.Color(0, 153, 153));
        combo.setFont(new java.awt.Font("Comic Sans MS", 1, 15)); // NOI18N
        combo.setForeground(new java.awt.Color(0, 0, 0));
        combo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "< seleciona >", "Gerente", "Auditor", "Planificador", "Dibujante" }));
        combo.setActionCommand("");
        combo.setOpaque(false);
        combo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(combo, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(combo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 15, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 470, 80));

        jPanel1.setBackground(new java.awt.Color(255, 153, 51));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Especialidad", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 1, 18), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        chkIngci.setBackground(null);
        chkIngci.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        chkIngci.setForeground(new java.awt.Color(0, 0, 0));
        chkIngci.setText("Ingenieía civil");
        chkIngci.setOpaque(false);
        jPanel1.add(chkIngci, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 140, -1));

        chkIngind.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        chkIngind.setForeground(new java.awt.Color(0, 0, 0));
        chkIngind.setText("Ingeniería industrial");
        chkIngind.setOpaque(false);
        jPanel1.add(chkIngind, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, -1));

        chkGeren.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        chkGeren.setForeground(new java.awt.Color(0, 0, 0));
        chkGeren.setText("Gerencia de proyectos");
        chkGeren.setOpaque(false);
        jPanel1.add(chkGeren, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 230, 160));

        jPanel2.setBackground(new java.awt.Color(0, 51, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Idioma ingles", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 1, 18), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        radios2.add(r1);
        r1.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        r1.setForeground(new java.awt.Color(0, 0, 0));
        r1.setText("Basico");
        r1.setOpaque(false);
        jPanel2.add(r1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 40, 100, -1));

        radios2.add(r2);
        r2.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        r2.setForeground(new java.awt.Color(0, 0, 0));
        r2.setText("Intermedio");
        r2.setOpaque(false);
        jPanel2.add(r2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, -1, -1));

        radios2.add(r3);
        r3.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        r3.setForeground(new java.awt.Color(0, 0, 0));
        r3.setText("Avanzado");
        r3.setOpaque(false);
        jPanel2.add(r3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 120, -1, -1));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 20, 220, 160));

        jPanel3.setBackground(new java.awt.Color(0, 153, 153));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resultado de evalucion", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 1, 18), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtTOL.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        txtTOL.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtTOL.setEnabled(false);
        jPanel3.add(txtTOL, new org.netbeans.lib.awtextra.AbsoluteConstraints(248, 44, 191, -1));

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Puntaje obtenido");
        jPanel3.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 45, 188, 24));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, 470, 90));

        jPanel5.setBackground(new java.awt.Color(102, 102, 255));
        jPanel5.setForeground(new java.awt.Color(102, 102, 255));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setBackground(new java.awt.Color(255, 153, 0));
        jButton1.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 0, 0));
        jButton1.setText("limpiar");
        jPanel5.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 390, -1, -1));

        jButton2.setBackground(new java.awt.Color(204, 0, 0));
        jButton2.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        jButton2.setForeground(new java.awt.Color(0, 0, 0));
        jButton2.setText("regresar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 390, -1, -1));

        boton.setBackground(new java.awt.Color(51, 153, 0));
        boton.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        boton.setForeground(new java.awt.Color(0, 0, 0));
        boton.setText("calcular");
        boton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonActionPerformed(evt);
            }
        });
        jPanel5.add(boton, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 390, -1, -1));

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 510, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

   
    
    private void botonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonActionPerformed
     // se crea una variable double
        double A=0;
     //    
    try{ 
        ////// si los checks estan selecionados/////
        if(chkIngci.isSelected())
      {
        A=A+10;  
      }
         if(chkIngind.isSelected())
      {
        A=A+8;  
      } 
         if(chkGeren.isSelected())
      {
        A=A+5;  
      } 
      ///// si los radio buttons estan selecionados///   
         if(r1.isSelected())
         {
             A=A+2;
         }else if(r2.isSelected())
         {
            A=A+4; 
         }else if (r3.isSelected())
         {
             A= A+6;
         }
      /// comparar selecion del combo y darle un valor////           
        if(c.equals("< seleciona >"))
         {
             A = A + 0;
         }
         if(c.equals("Gerente"))
         {
             A = A + 5;
         }  
         if(c.equals("Auditor"))
         {
             A = A + 4;
         }
         if(c.equals("Planificador"))
         {
             A = A + 7;
         }
         if(c.equals("Dibujante"))
         {
             A = A + 3;
         }
        ///// asignar total a txtTOL
        txtTOL.setText(String.valueOf(A));
        
     }catch(Exception e)
     {
        JOptionPane.showMessageDialog(null, "Error: Revisa los campos :(\n"+"     Vuelve a intentalo  :3 ","Error",JOptionPane.ERROR_MESSAGE);
     }//                                    "                          " "                          "   
    }//GEN-LAST:event_botonActionPerformed

    private void comboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboActionPerformed
       // se crea un objeto 
        JComboBox cb = (JComboBox)evt.getSource();
        c = (String) cb.getSelectedItem();
        
        
        
    }//GEN-LAST:event_comboActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       
        SEE S = new SEE();
        S.setVisible(true);
        this.dispose();
        
        
    }//GEN-LAST:event_jButton2ActionPerformed

    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Puntaje.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Puntaje.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Puntaje.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Puntaje.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Puntaje().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton;
    private javax.swing.JCheckBox chkGeren;
    private javax.swing.JCheckBox chkIngci;
    private javax.swing.JCheckBox chkIngind;
    private javax.swing.JComboBox<String> combo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JRadioButton r1;
    private javax.swing.JRadioButton r2;
    private javax.swing.JRadioButton r3;
    private javax.swing.ButtonGroup radios2;
    private javax.swing.JTextField txtTOL;
    // End of variables declaration//GEN-END:variables
}
